#include <stdio.h>
#include <stdlib.h>

struct node {
  int data;
  struct node *next;
} *head;

void insert(int data) {
  struct node *temp = head;
  while( temp->next != NULL ) {
      temp = temp->next;
  }
  struct node *temp2 = (struct node*) malloc(sizeof(struct node));
  temp2->data = data;
  temp2->next = NULL;
  temp->next = temp2;
}

void display() {
  struct node *temp = head->next;
  printf("\n");
  while(temp != NULL) {
    printf("%d ", temp->data);
    temp = temp->next;
  }
  printf("\n");
}

void displayrev(struct node *link) {
  if (link != NULL)
    displayrev(link->next);
  printf("%d ", link->data);
}

int main(int argc, char const *argv[]) {
  head = ( struct node* )malloc(sizeof(struct node));
  head->next = NULL;

  int ch,data;
  do {
    printf("1. Insert\n2. Display\n3. Display(reverse)\n4. Exit\n :");
    scanf("%d", &ch);
    switch (ch) {
      case 1:
        printf("Enter the data: ");
        scanf("%d", &data);
        insert(data);
        break;
      case 2:
        display();
        break;
      case 3:
        displayrev(head->next);
        break;
      case 4:
        printf("Exiting..");
        break;
      default:
        printf("Invalid choice\n");
        break;
    }
  } while (ch != 3);
  return 0;
}