#include <stdio.h>
#include <stdlib.h>

struct node {
  int data;
  struct node *next;
} *head;

void insertFront(int data) {
  struct node *link = (struct node*) malloc(sizeof(struct node));
  link->data = data;
  link->next = head->next;
  head->next = link;
}

void display() {
  struct node *temp = head->next;
  printf("\n");
  while(temp != NULL) {
    printf("%d\n", temp->data);
    temp = temp->next;
  }
  printf("\n");
}

int main(int argc, char const *argv[]) {
  head = (struct node*) malloc(sizeof(struct node));
  head->next = NULL;
  int choice, data;
  do {
    printf("1. Insert at Front\n2. Display\n3. Exit\n");
    scanf("%d", &choice);
    switch(choice) {
      case 1: printf("Enter Data: ");
              scanf("%d", &data);
              insertFront(data);
              break;
      case 2: display();
              break;
      case 3: printf("Exiting\n");
              break;
      default:printf("Invalid Choice\n" );
              break;
    }
  } while (choice!=3);
  return 0;
}