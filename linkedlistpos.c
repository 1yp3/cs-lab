#include <stdio.h>
#include <stdlib.h>

struct node {
    int data;
    struct node *next;
} *head;

void insert(int data) {
    struct node *temp = head;
    while( temp->next != NULL ) {
        temp = temp->next;
    }
    struct node *temp2 = (struct node*) malloc(sizeof(struct node));
    temp2->data = data;
    temp2->next = NULL;
    temp->next = temp2;
}

void insertAfter(int data, int pos) {
    struct node *temp = head;
    while( temp->data != pos ) {
        temp = temp->next;
    }
    struct node *newNode = (struct node*) malloc(sizeof(struct node));
    newNode->data = data;
    newNode->next = temp->next;
    temp->next = newNode;
}

void display() {
    struct node *temp = head->next;
    printf("\n");
    while( temp != NULL ) {
        printf("%d\n", temp->data);
        temp = temp->next;
    }
    printf("\n");
}

int main() {
    head = (struct node*) malloc(sizeof(struct node));
    head->next = NULL;

    int choice, data, pos;
    do{
        printf("\n1. Insert\n2. Insert after _\n3. Display\n4. Exit\n: ");
        scanf("%d", &choice);
        switch(choice) {
            case 1: printf("Enter the data: ");
                    scanf("%d", &data);
                    insert(data);
                    break;
            case 2: printf("Insert after(data): ");
                    scanf("%d", &pos);
                    printf("Enter the data: ");
                    scanf("%d", &data);
                    insertAfter(data, pos);
                    break;
            case 3: display();
                    break;
            case 4: break;
            default:printf("Invalid choice");
                    break;
        }
    }while(choice != 4);
}