#include<stdio.h>
char hash[30];

int hashFunc(char key) {
	int index = ( key % 29) + 1;
	return index;
}

int main() {
	int k;
	for(k=0; k<30; k++) hash[k]=-1;
	char key[25];
	int i=0, j, index, len;
	printf( "Enter the Key Value: " );
	scanf( "%s", key );
	for( len = 0; key[len] != '\0'; len++ );
	for( i = 0; i < len; i ++ ) {
		index = hashFunc( key[i] );
		if( hash[index] <= 0 )
			hash[index] = key[i];
		else {
			j = index + 1;
			while( j != index ) {
				if( hash[j] <= 0 ) {
					hash[j] = key[i];
					break;
				}

				if( j == 29 )
					j = 0;
				else 
					j ++;
			}
		}
	}
	printf("Index\tValue\n");
	for( i = 0; i<30; i++) {
		if( hash[i] == -1 ) {
			printf("%d\n", i);
		}
		else {
			printf("%d\t%c\n", i, hash[i]);
		}
	}
	printf( "\n" );
	return 0;
}
