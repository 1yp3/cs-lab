#include <stdio.h>
#include <stdlib.h>

struct node {
  int data;
  struct node *next;
} *head;

void insert(int data) {
  struct node *temp = head;
  while( temp->next != NULL ) {
    temp = temp->next;
  }
  struct node *temp2 = (struct node*) malloc(sizeof(struct node));
  temp2->data = data;
  temp2->next = NULL;
  temp->next = temp2;
}

int count() {
  int i=0;
  struct node *temp = head;
  while( temp->next != NULL )
    { temp = temp->next;
      i++;    }
  return i;
}

void main() {
  head = ( struct node* ) malloc(sizeof(struct node));
  head->next = NULL;
  int choice, data;
  do {
    printf("1. Insert\n2. Count\n3. Exit\n\n : ");
    scanf("%d", &choice);
    switch (choice) {
      case 1:
        printf("Enter the data: ");
        scanf("%d", &data);
        insert(data);
        break;
      case 2:
        printf("There are %d nodes in the linked list\n\n", count());
        break;
      case 3:
        break;
    }
  } while(choice != 3);
}